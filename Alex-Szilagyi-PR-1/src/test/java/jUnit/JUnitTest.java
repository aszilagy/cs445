/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jUnit;

import Servlets.Orders;
import Servlets.Reports;
import Servlets.Seating;
import Servlets.Shows;
import Servlets.Tickets;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import javaCode.DonationInfo;
import javaCode.OrderInfo;
import javaCode.PatronInfo;
import javaCode.ReportInfo;
import javaCode.SeatingInfo;
import javaCode.Rows;
import javaCode.Seats;
import javaCode.ShowInfo;
import javaCode.TempData;
import javaCode.TicketInfo;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;

/**
 *
 * @author alex
 */
public class JUnitTest extends Mockito {

    public JUnitTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // @Test
    // public void hello() {}
    @Test
    public void testPatronInfo() {
        PatronInfo p = new PatronInfo("Bob", "888-888-8888", "bob@bob.com", "123 Main Street", "xxxxxxxxx9999", "11-5-2020");

        assertEquals(p.name, "Bob");
        assertEquals(p.phone, "888-888-8888");
        assertEquals(p.email, "bob@bob.com");
        assertEquals(p.billing_address, "123 Main Street");
        assertEquals(p.cc_number, "xxxxxxxxx9999");
        assertEquals(p.cc_expiration_date, "11-5-2020");
    }

    @Test
    public void testSeatCreation() {

        List<Seats> seatOne = new ArrayList<>();
        Seats seat1 = new Seats("201", "1", "available");
        seatOne.add(seat1);

        Rows s = new Rows("101", seatOne);

        assertEquals(s.row, "101");
        //.assertEquals(s.seatList, seatOne);
    }

    @Test
    public void testSeatingInfoCreation() {
        List<Seats> seatOne = new ArrayList<>();
        Seats seat1 = new Seats("201", "1", "available");
        seatOne.add(seat1);

        Rows s = new Rows("101", seatOne);

        Rows[] seatArr = new Rows[1];
        seatArr[0] = s;

        SeatingInfo testSeatingInfo = new SeatingInfo("307", 25, "100", "Front center", seatArr);

        assertEquals(testSeatingInfo.wid, "307");
        assertEquals(testSeatingInfo.price, 25);
        assertEquals(testSeatingInfo.sid, "100");
        assertEquals(testSeatingInfo.name, "Front center");
        Assert.assertArrayEquals(testSeatingInfo.section, seatArr);
        assertEquals(testSeatingInfo.section[0], s);
    }

    @Test
    public void testShowInfoCreation() {
        List<Seats> seatOne = new ArrayList<>();
        Seats seat1 = new Seats("201", "1", "available");
        seatOne.add(seat1);

        Rows s = new Rows("101", seatOne);

        Rows[] seatArr = new Rows[1];
        seatArr[0] = s;

        SeatingInfo testSeatingInfo = new SeatingInfo("307", 25, "100", "Front center", seatArr);

        List<SeatingInfo> testSeatingList = new ArrayList<>();
        testSeatingList.add(testSeatingInfo);

        ShowInfo showTest = new ShowInfo("103", "testShow", "www.testShow.com", "11-1-15", "13:00", testSeatingList);

        assertEquals(showTest.wid, "103");
        assertEquals(showTest.name, "testShow");
        assertEquals(showTest.web, "www.testShow.com");
        assertEquals(showTest.date, "11-1-15");
        assertEquals(showTest.time, "13:00");

        assertEquals(showTest.seatInfo, testSeatingList);
    }

    @Test
    public void testTicketInfoCreation() {
        List<Seats> seatOne = new ArrayList<>();
        Seats seat1 = new Seats("201", "1", "available");
        seatOne.add(seat1);

        Rows s = new Rows("101", seatOne);

        Rows[] seatArr = new Rows[1];
        seatArr[0] = s;

        SeatingInfo testSeatingInfo = new SeatingInfo("307", 25, "100", "Front center", seatArr);

        List<SeatingInfo> testSeatingList = new ArrayList<>();
        testSeatingList.add(testSeatingInfo);

        ShowInfo showTest = new ShowInfo("103", "testShow", "www.testShow.com", "11-1-15", "13:00", testSeatingList);

        TicketInfo ticketTest = new TicketInfo("1", "available", 30, "307", testSeatingInfo, showTest);

        assertEquals(ticketTest.tid, "1");
        assertEquals(ticketTest.status, "available");
        assertEquals(ticketTest.price, 30);
        assertEquals(ticketTest.wid, "307");

        //FIXME: Change these to test the individual attributes?
        assertEquals(ticketTest.seatInfo, testSeatingInfo);
        assertEquals(ticketTest.showIn, showTest);
    }

    @Test
    public void testOrderInfoCreation() {
        PatronInfo p = new PatronInfo("Bob", "888-888-8888", "bob@bob.com", "123 Main Street", "xxxxxxxxx9999", "11-5-2020");

        List<Seats> seatOne = new ArrayList<>();
        Seats seat1 = new Seats("201", "1", "available");
        seatOne.add(seat1);

        Rows s = new Rows("101", seatOne);

        Rows[] seatArr = new Rows[1];
        seatArr[0] = s;

        SeatingInfo testSeatingInfo = new SeatingInfo("307", 25, "100", "Front center", seatArr);

        List<SeatingInfo> testSeatingList = new ArrayList<>();
        testSeatingList.add(testSeatingInfo);

        ShowInfo showTest = new ShowInfo("103", "testShow", "www.testShow.com", "11-1-15", "13:00", testSeatingList);

        TicketInfo ticketTest = new TicketInfo("1", "available", 30, "307", testSeatingInfo, showTest);

        //FIXME: Change this to a list in OrderInfo
        TicketInfo[] ticketArrayTest = new TicketInfo[]{ticketTest};

        OrderInfo o1 = new OrderInfo("3", showTest, "1-1-15", 3, 3, p, ticketArrayTest);

        assertEquals(o1.oid, "3");
        assertEquals(o1.showInformation, showTest);
        assertEquals(o1.date_ordered, "1-1-15");
        assertEquals(o1.order_amount, 3);
        assertEquals(o1.numOfTickets, 3);
        assertEquals(o1.patronInformation, p);
        Assert.assertArrayEquals(o1.tickets, ticketArrayTest);
    }

    @Test
    public void testDonationInfoCreation() {
        PatronInfo p = new PatronInfo("Bob", "888-888-8888", "bob@bob.com", "123 Main Street", "xxxxxxxxx9999", "11-5-2020");

        List<Seats> seatOne = new ArrayList<>();
        Seats seat1 = new Seats("201", "1", "available");
        seatOne.add(seat1);

        Rows s = new Rows("101", seatOne);

        Rows[] seatArr = new Rows[1];
        seatArr[0] = s;

        SeatingInfo testSeatingInfo = new SeatingInfo("307", 25, "100", "Front center", seatArr);

        List<SeatingInfo> testSeatingList = new ArrayList<>();
        testSeatingList.add(testSeatingInfo);

        ShowInfo showTest = new ShowInfo("103", "testShow", "www.testShow.com", "11-1-15", "13:00", testSeatingList);

        TicketInfo ticketTest = new TicketInfo("1", "available", 30, "307", testSeatingInfo, showTest);
        TicketInfo[] ticketArrayTest = new TicketInfo[]{ticketTest};

        OrderInfo o1 = new OrderInfo("1", showTest, "1-1-15", 3, 3, p, ticketArrayTest);

        TempData.orderArr.add(o1);
        //FIXME: Change this to a list in OrderInfo

        DonationInfo d1 = new DonationInfo("105", 1, "pending", ticketArrayTest, p);

        assertEquals(d1.did, "105");
        assertEquals(d1.countReq, 1);
        assertEquals(d1.donationStatus, "pending");
        Assert.assertArrayEquals(d1.ticketIn, ticketArrayTest);
        assertEquals(d1.patronIn, p);
    }

    @Test
    public void testTicketInfoSetStatus() {
        List<Seats> seatOne = new ArrayList<>();
        Seats seat1 = new Seats("201", "1", "available");
        seatOne.add(seat1);

        Rows s = new Rows("101", seatOne);

        Rows[] seatArr = new Rows[1];
        seatArr[0] = s;

        SeatingInfo testSeatingInfo = new SeatingInfo("307", 25, "100", "Front center", seatArr);

        List<SeatingInfo> testSeatingList = new ArrayList<>();
        testSeatingList.add(testSeatingInfo);

        ShowInfo showTest = new ShowInfo("103", "testShow", "www.testShow.com", "11-1-15", "13:00", testSeatingList);

        TicketInfo ticketTest = new TicketInfo("1", "available", 30, "307", testSeatingInfo, showTest);

        assertEquals(ticketTest.status, "available");

        ticketTest.setStatus("open");
        assertEquals(ticketTest.getStatus(), "open");
    }

    @Test
    public void testDonationInfoSetStatus() {
        PatronInfo p = new PatronInfo("Bob", "888-888-8888", "bob@bob.com", "123 Main Street", "xxxxxxxxx9999", "11-5-2020");

        List<Seats> seatOne = new ArrayList<>();
        Seats seat1 = new Seats("201", "1", "available");
        seatOne.add(seat1);

        Rows s = new Rows("101", seatOne);

        Rows[] seatArr = new Rows[1];
        seatArr[0] = s;

        SeatingInfo testSeatingInfo = new SeatingInfo("307", 25, "100", "Front center", seatArr);

        List<SeatingInfo> testSeatingList = new ArrayList<>();
        testSeatingList.add(testSeatingInfo);

        ShowInfo showTest = new ShowInfo("103", "testShow", "www.testShow.com", "11-1-15", "13:00", testSeatingList);

        TicketInfo ticketTest = new TicketInfo("1", "available", 30, "307", testSeatingInfo, showTest);

        assertEquals(ticketTest.status, "available");
        TicketInfo[] ticketArrayTest = new TicketInfo[]{ticketTest};

        DonationInfo d1 = new DonationInfo("105", 1, "pending", ticketArrayTest, p);

        d1.setStatus("open");
        assertEquals(d1.getStatus(), "open");
    }

    @Test
    public void testReportInfo() {
        List<Seats> seatOne = new ArrayList<>();
        Seats seat1 = new Seats("201", "1", "available");
        seatOne.add(seat1);

        Rows s = new Rows("101", seatOne);

        Rows[] seatArr = new Rows[1];
        seatArr[0] = s;

        SeatingInfo testSeatingInfo = new SeatingInfo("307", 25, "100", "Front center", seatArr);
        List<SeatingInfo> testSeatingList = new ArrayList<>();
        List<ShowInfo> showTestList = new ArrayList<>();
        testSeatingList.add(testSeatingInfo);
        ShowInfo showTest = new ShowInfo("103", "testShow", "www.testShow.com", "11-1-15", "13:00", testSeatingList);
        showTestList.add(showTest);

        ReportInfo testRep = new ReportInfo("1", "TestReport", "11-1-15", "11-5-15", 3, 68, 66, "5%", showTestList);

        assertEquals(testRep.mrid, "1");
        assertEquals(testRep.reportName, "TestReport");
        assertEquals(testRep.startDate, "11-1-15");
        assertEquals(testRep.endDate, "11-5-15");
        assertEquals(testRep.totalShows, 3);
        assertEquals(testRep.totalSeats, 68);
        assertEquals(testRep.soldSeats, 66);
        assertEquals(testRep.overallOccupancy, "5%");
        assertEquals(testRep.showsHere, showTestList);

    }

    @Test
    public void testSeatingGet() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        when(response.getWriter()).thenReturn(outE);

        new Seating().doGet(request, response);

        String outString = stW.toString();
        //assertTrue(outString.contains("[{\"section_name\":\"Front right\",\"sid\":\"123\"},{\"section_name\":\"Front center\",\"sid\":\"124\"},{\"section_name\":\"Front left\",\"sid\":\"125\"},{\"section_name\":\"Main right\",\"sid\":\"126\"},{\"section_name\":\"Main center\",\"sid\":\"127\"},{\"section_name\":\"Main left\",\"sid\":\"128\"}]"));
    }

    @Test
    public void testNoShowGet() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        when(response.getWriter()).thenReturn(outE);

        new Shows().doGet(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains(""));
    }

    @Test
    public void testStartOrderGet() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        when(response.getWriter()).thenReturn(outE);

        new Orders().doGet(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains("[{\"patron_info\":{\"cc_expiration_date\":\"11-5-2020\",\"phone\":\"888-888-8888\",\"name\":\"Bob\",\"billing_address\":\"123 Main Street\",\"cc_number\":\"xxxxxxxxx9999\",\"email\":\"bob@bob.com\"},\"number_of_tickets\":3,\"wid\":\"103\",\"order_amount\":3,\"show_info\":{\"date\":\"11-1-15\",\"web\":\"www.testShow.com\",\"name\":\"testShow\",\"time\":\"13:00\"},\"oid\":\"1\",\"date_ordered\":\"1-1-15\"}]"));
    }

    @Test
    public void testStartTicketsGet() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        when(response.getWriter()).thenReturn(outE);

        new Tickets().doGet(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains(""));
    }

    @Test
    public void testStartReportsGet() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        when(response.getWriter()).thenReturn(outE);

        new Reports().doGet(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains("[{\"mrid\":\"900\",\"name\":\"Test Report 1\"}]"));
    }

    public void addTempData() {
        PatronInfo p = new PatronInfo("Bob", "888-888-8888", "bob@bob.com", "123 Main Street", "xxxxxxxxx9999", "11-5-2020");

        List<Seats> seatOne = new ArrayList<>();
        Seats seat1 = new Seats("201", "1", "available");
        seatOne.add(seat1);

        Rows s = new Rows("101", seatOne);

        Rows[] seatArr = new Rows[1];
        seatArr[0] = s;

        SeatingInfo testSeatingInfo = new SeatingInfo("103", 25, "100", "Front center", seatArr);

        List<SeatingInfo> testSeatingList = new ArrayList<>();
        testSeatingList.add(testSeatingInfo);
        ShowInfo showTest = new ShowInfo("103", "testShow", "www.testShow.com", "11-1-15", "13:00", testSeatingList);

        TempData.showArr.add(showTest);
        TempData.seatArr.add(testSeatingInfo);

        TicketInfo ticketTest = new TicketInfo("1", "available", 30, "307", testSeatingInfo, showTest);

        TicketInfo[] ticketArrayTest = new TicketInfo[]{ticketTest};

        OrderInfo o1 = new OrderInfo("1", showTest, "1-1-15", 3, 3, p, ticketArrayTest);

        TempData.orderArr.add(o1);

        DonationInfo d1 = new DonationInfo("105", 1, "pending", ticketArrayTest, p);

        TempData.donationArr.add(d1);
    }

    @Test
    public void testShowsGet() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        TempData.createStuff();

        when(response.getWriter()).thenReturn(outE);

        when(request.getPathInfo()).thenReturn("");

        new Shows().doGet(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains(""));
    }

    @Test
    public void testShowsPostWidFail() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        TempData.createStuff();

        when(response.getWriter()).thenReturn(outE);

        when(request.getPathInfo()).thenReturn("/307");

        new Shows().doGet(request, response);

        String outString = stW.toString();
        //assertTrue(outString.contains(""));
    }

    @Test
    public void testShowsPostWidPass() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        TempData.createStuff();

        when(response.getWriter()).thenReturn(outE);

        when(request.getPathInfo()).thenReturn("/103");

        new Shows().doGet(request, response);

        String outString = stW.toString();
        //assertTrue(outString.contains("{\"wid\":\"103\",\"seating_info\":[{\"price\":25,\"sid\":\"100\"},{\"price\":25,\"sid\":\"100\"},{\"price\":25,\"sid\":\"100\"},{\"price\":25,\"sid\":\"100\"},{\"price\":25,\"sid\":\"100\"},{\"price\":25,\"sid\":\"100\"},{\"price\":25,\"sid\":\"100\"},{\"price\":25,\"sid\":\"100\"},{\"price\":25,\"sid\":\"100\"}],\"show_info\":{\"date\":\"11-1-15\",\"web\":\"www.testShow.com\",\"name\":\"testShow\",\"time\":\"13:00\"}}"));
    }

    @Test
    public void testShowsGetSection() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        TempData.createStuff();

        when(response.getWriter()).thenReturn(outE);

        when(request.getPathInfo()).thenReturn("/103/sections");

        new Shows().doGet(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains(""));
    }

    @Test
    public void testShowsGetSectionIndiv() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        TempData.createStuff();

        when(response.getWriter()).thenReturn(outE);

        when(request.getPathInfo()).thenReturn("/103/sections/100");

        new Shows().doGet(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains(""));
    }

    @Test
    public void testShowsGetDonation() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        TempData.createStuff();

        when(response.getWriter()).thenReturn(outE);

        when(request.getPathInfo()).thenReturn("/105/donations/105");

        new Shows().doGet(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains(""));
    }

    @Test
    public void testOrdersGet() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        TempData.createStuff();

        when(response.getWriter()).thenReturn(outE);

        when(request.getPathInfo()).thenReturn("");

        new Orders().doGet(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains(""));
    }

    @Test
    public void testOrdersGetId() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        TempData.createStuff();

        when(response.getWriter()).thenReturn(outE);

        when(request.getPathInfo()).thenReturn("/1");

        new Orders().doGet(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains(""));
    }

    @Test
    public void testOrdersGetIdDate() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        TempData.createStuff();

        when(request.getParameter("start_date")).thenReturn("20150105");
        when(request.getParameter("end_date")).thenReturn("20150109");

        when(response.getWriter()).thenReturn(outE);

        when(request.getPathInfo()).thenReturn("");

        new Orders().doGet(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains(""));
    }

    @Test
    public void testOrdersStartDate() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        TempData.createStuff();

        when(request.getParameter("start_date")).thenReturn("20150105");
        when(request.getParameter("end_date")).thenReturn("20150109");

        when(response.getWriter()).thenReturn(outE);

        when(request.getPathInfo()).thenReturn("?start_date=20150105&end_date=20150109");

        new Orders().doGet(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains(""));
    }

    @Test
    public void testTicketGet() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        TempData.createStuff();

        when(response.getWriter()).thenReturn(outE);

        when(request.getPathInfo()).thenReturn("/1");

        new Tickets().doGet(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains(""));
    }

    /*@Test
    public void testTicketPost() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        addTempData();

        when(request.getPathInfo()).thenReturn("");
        when(response.getWriter()).thenReturn(outE);

        File file = new File("./CS-445-project_test/jsons/test27.json");
        BufferedReader readerNew = new BufferedReader(new FileReader(file));

        when(request.getReader()).thenReturn(readerNew);
        when(request.getPathInfo()).thenReturn("/donations");

        new Tickets().doPost(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains(""));
    }

    @Test
    public void testTicketPostTwo() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        addTempData();

        when(request.getPathInfo()).thenReturn("");
        when(response.getWriter()).thenReturn(outE);

        File file = new File("./CS-445-project_test/jsons/test27.json");
        BufferedReader readerNew = new BufferedReader(new FileReader(file));

        when(request.getReader()).thenReturn(readerNew);
        when(request.getPathInfo()).thenReturn("/notDon");

        new Tickets().doPost(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains(""));
    }*/

    public void testShowPut() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        TempData.createStuff();

        when(request.getPathInfo()).thenReturn("");
        when(response.getWriter()).thenReturn(outE);

        File file = new File("./CS-445-project_test/jsons/test7.json");
        BufferedReader readerNew = new BufferedReader(new FileReader(file));

        when(request.getReader()).thenReturn(readerNew);
        when(request.getPathInfo()).thenReturn("/103");

        new Shows().doPut(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains(""));
    }

    /*@Test
    public void testOrderPost() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        addTempData();

        when(request.getPathInfo()).thenReturn("");
        when(response.getWriter()).thenReturn(outE);

        File file = new File("./CS-445-project_test/jsons/test18.json");
        BufferedReader readerNew = new BufferedReader(new FileReader(file));

        when(request.getReader()).thenReturn(readerNew);

        new Orders().doPost(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains(""));
    }*/

    /*@Test
    public void testShowPost() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        addTempData();

        when(request.getPathInfo()).thenReturn("/107/donations");
        when(response.getWriter()).thenReturn(outE);

        File file = new File("./CS-445-project_test/jsons/test23.json");
        BufferedReader readerNew = new BufferedReader(new FileReader(file));

        when(request.getReader()).thenReturn(readerNew);

        new Shows().doPost(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains(""));
    }

    @Test
    public void testShowPostDonationTwo() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        addTempData();

        when(request.getPathInfo()).thenReturn("/107/not");
        when(response.getWriter()).thenReturn(outE);

        File file = new File("./CS-445-project_test/jsons/test24.json");
        BufferedReader readerNew = new BufferedReader(new FileReader(file));

        when(request.getReader()).thenReturn(readerNew);

        new Shows().doPost(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains(""));
    }

    @Test
    public void testShowPostDonations() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        addTempData();

        when(request.getPathInfo()).thenReturn(null);
        when(response.getWriter()).thenReturn(outE);

        File file = new File("./CS-445-project_test/jsons/test4.json");
        BufferedReader readerNew = new BufferedReader(new FileReader(file));

        when(request.getReader()).thenReturn(readerNew);

        new Shows().doPost(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains(""));
    }*/

    @Test
    public void testSeatingGetNone() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        TempData.createStuff();

        when(request.getPathInfo()).thenReturn(null);
        when(request.getParameter("show")).thenReturn("103");
        when(request.getParameter("section")).thenReturn("100");
        when(request.getParameter("count")).thenReturn("1");
        when(request.getParameter("starting_seat_id")).thenReturn("103");

        when(response.getWriter()).thenReturn(outE);

        new Seating().doGet(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains(""));
    }

    @Test
    public void testSeatingGetNoneStarting() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        TempData.createStuff();

        when(request.getPathInfo()).thenReturn(null);
        when(request.getParameter("show")).thenReturn("103");
        when(request.getParameter("section")).thenReturn("100");
        when(request.getParameter("count")).thenReturn("5");
        when(request.getParameter("starting_seat_id")).thenReturn(null);

        when(response.getWriter()).thenReturn(outE);

        new Seating().doGet(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains(""));
    }

    @Test
    public void testSeatingGetRegular() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StringWriter stW = new StringWriter();
        PrintWriter outE = new PrintWriter(stW);

        TempData.createStuff();

        when(request.getPathInfo()).thenReturn("/100");

        when(response.getWriter()).thenReturn(outE);

        new Seating().doGet(request, response);

        String outString = stW.toString();
        assertTrue(outString.contains(""));
    }
}
