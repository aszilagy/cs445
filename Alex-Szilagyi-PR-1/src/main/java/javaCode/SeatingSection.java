/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaCode;

/**
 *
 * @author alex
 */
public class SeatingSection {
    public String wid;
    public String sid;
    
    public String section_name;
    public String row;
    public Seats[] indivSeats;
    
    public SeatingSection(String secName, String rw, Seats[] seatArr){
        section_name = secName;
        row = rw;
        indivSeats = seatArr;
    }
    
}
