/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaCode;

/**
 *
 * @author alex
 */
public class DonationInfo {
    public String did;
    public int countReq;
    public String donationStatus;
    public TicketInfo[] ticketIn;
    public PatronInfo patronIn;
    
    public DonationInfo(String donID, int countRequested, String donStat, TicketInfo[] tickIn, PatronInfo patIn){
        did = donID;
        countReq = countRequested;
        donationStatus = donStat;
        ticketIn = tickIn;
        patronIn = patIn;
    }
    
    public void setStatus(String newStat){
        donationStatus = newStat;
    }
    
    public String getStatus(){
        return donationStatus;
    }
}
