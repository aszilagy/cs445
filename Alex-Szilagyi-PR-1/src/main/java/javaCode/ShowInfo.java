/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaCode;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 *
 * @author alex
 */
public class ShowInfo {
    public String wid;
    public String name;
    public String web;
    //FIXME: CHANGE THESE BACK to Date / SimpleDateFormat ? 
    public String date;
    public String time;
    public List<SeatingInfo> seatInfo;
//    public Date date = new Date();
//    public SimpleDateFormat time = new SimpleDateFormat("HH:mm", Locale.US);
    
    public ShowInfo(String w, String n, String we, String da, String t, List<SeatingInfo> seIn)
    {
        wid = w;
        name = n;
        web = we;
        date = da;
        time = t;
        seatInfo = seIn;
    }
    
}
