/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaCode;

import java.util.List;

/**
 *
 * @author alex
 */
public class ReportInfo {
    public String mrid;
    public String reportName;
    public String startDate;
    public String endDate;
    public int totalShows; //FIXME : These should reflect the total seats from all shows
    public int totalSeats;
    public int soldSeats;
    public String overallOccupancy; //FIXME : Should be soldSeats / seatsAvailable
    public List<ShowInfo> showsHere;
    
    public ReportInfo(String mr, String rep, String st, String en, int totShow, int totSeat, int solSeat, String ovOcc, List<ShowInfo> shoHe){
        mrid = mr;
        reportName = rep;
        startDate = st;
        endDate = en;
        totalShows = totShow;
        totalSeats = totSeat;
        soldSeats = solSeat;
        overallOccupancy = ovOcc;
        showsHere = shoHe;
    }
}
