/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaCode;

/**
 *
 * @author alex
 */
public class TicketInfo {
    public String tid;
    public String status;
    public int price;

    //FIXME: Remove WID and other showInfo
    public String wid;
    public SeatingInfo seatInfo;
    public ShowInfo showIn;
    
    public TicketInfo(String tidNo, String sta, int pri, String wi, SeatingInfo seIn, ShowInfo shIn){
        tid = tidNo;
        status = sta;
        price = pri;
        wid = wi;
        seatInfo = seIn;
        showIn = shIn;
    }
    
    public void setStatus(String newStat){
        status = newStat;
    }
    
    public String getStatus(){
        return status;
    }

}
