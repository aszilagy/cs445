/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javaCode.OrderInfo;
import javaCode.PatronInfo;
import javaCode.SeatingInfo;
import javaCode.Rows;
import javaCode.Seats;
import javaCode.ShowInfo;
import javaCode.TempData;
import javaCode.TicketInfo;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author alex
 */
@WebServlet(name = "Orders", urlPatterns = {"/thalia/orders/*"})
public class Orders extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        if (TempData.counterSeating == 0) {
            TempData.createStuff();
            TempData.counterSeating++;
        } else {
            TempData.counterSeating++;
        }
        String pathInfo = request.getPathInfo(); // /{value}/test

        if (pathInfo != null) {

            String[] pathParts = pathInfo.split("/");
            int numParts = pathParts.length;

            if (numParts == 2 && pathParts[1] != null) {
                String orderID = pathParts[1];
                JSONObject mainObj = new JSONObject();
                for (OrderInfo o : TempData.orderArr) {
                    if (orderID.equals(o.oid)) {
                        mainObj = new JSONObject();
                        mainObj.put("oid", o.oid);
                        mainObj.put("wid", o.showInformation.wid);

                        JSONObject showInfo = new JSONObject();
                        showInfo.put("name", o.showInformation.name);
                        showInfo.put("web", o.showInformation.web);
                        showInfo.put("date", o.showInformation.date);
                        showInfo.put("time", o.showInformation.time);

                        mainObj.put("date_ordered", o.date_ordered);
                        mainObj.put("order_amount", o.order_amount);

                        JSONObject patronInfo = new JSONObject();
                        patronInfo.put("name", o.patronInformation.name);
                        patronInfo.put("phone", o.patronInformation.phone);
                        patronInfo.put("email", o.patronInformation.email);
                        patronInfo.put("billing_address", o.patronInformation.billing_address);
                        patronInfo.put("cc_number", o.patronInformation.cc_number);
                        patronInfo.put("cc_expiration_date", o.patronInformation.cc_expiration_date);

                        JSONArray ticketArr = new JSONArray();

                        for (TicketInfo t : o.tickets) {
                            JSONObject ticketObj = new JSONObject();
                            ticketObj.put("tid", t.tid);
                            ticketObj.put("status", t.status);

                            ticketArr.add(ticketObj);
                        }

                        mainObj.put("show_info", showInfo);
                        mainObj.put("patron_info", patronInfo);
                        mainObj.put("tickets", ticketArr);
                    }
                    try (PrintWriter outE = response.getWriter()) {
                        outE.println(mainObj);
                        outE.flush();
                    }
                }
            }
        } else {

            String startDateCheck = request.getParameter("start_date");
            String endDateCheck = request.getParameter("end_date");

            if (startDateCheck != null && endDateCheck != null) {
                JSONArray outerArr = new JSONArray();

                for (OrderInfo o : TempData.orderArr) {
                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd", Locale.ENGLISH);
                    DateTimeFormatter secondDtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm", Locale.ENGLISH);
                    LocalDate startDate = LocalDate.parse(startDateCheck, dtf);
                    LocalDate endDate = LocalDate.parse(endDateCheck, dtf);
                    LocalDate dateOrder = LocalDate.parse(o.date_ordered, secondDtf);

                    if (dateOrder.isAfter(startDate) && dateOrder.isBefore(endDate)) {
                        JSONObject mainObj = new JSONObject();
                        mainObj.put("oid", o.oid);
                        mainObj.put("wid", o.showInformation.wid);

                        JSONObject showInfo = new JSONObject();
                        showInfo.put("name", o.showInformation.name);
                        showInfo.put("web", o.showInformation.web);
                        showInfo.put("date", o.showInformation.date);
                        showInfo.put("time", o.showInformation.time);

                        mainObj.put("date_ordered", o.date_ordered);
                        mainObj.put("order_amount", o.order_amount);
                        mainObj.put("number_of_tickets", o.numOfTickets);

                        JSONObject patronInfo = new JSONObject();
                        patronInfo.put("name", o.patronInformation.name);
                        patronInfo.put("phone", o.patronInformation.phone);
                        patronInfo.put("email", o.patronInformation.email);
                        patronInfo.put("billing_address", o.patronInformation.billing_address);
                        patronInfo.put("cc_number", o.patronInformation.cc_number);
                        patronInfo.put("cc_expiration_date", o.patronInformation.cc_expiration_date);

                        mainObj.put("show_info", showInfo);
                        mainObj.put("patron_info", patronInfo);

                        outerArr.add(mainObj);
                    }
                    try (PrintWriter outE = response.getWriter()) {
                        outE.println(outerArr);
                        outE.flush();
                    }
                }

            } else {
                JSONArray outerArr = new JSONArray();

                for (OrderInfo o : TempData.orderArr) {
                    JSONObject mainObj = new JSONObject();
                    mainObj.put("oid", o.oid);
                    mainObj.put("wid", o.showInformation.wid);

                    JSONObject showInfo = new JSONObject();
                    showInfo.put("name", o.showInformation.name);
                    showInfo.put("web", o.showInformation.web);
                    showInfo.put("date", o.showInformation.date);
                    showInfo.put("time", o.showInformation.time);

                    mainObj.put("date_ordered", o.date_ordered);
                    mainObj.put("order_amount", o.order_amount);
                    mainObj.put("number_of_tickets", o.numOfTickets);

                    JSONObject patronInfo = new JSONObject();
                    patronInfo.put("name", o.patronInformation.name);
                    patronInfo.put("phone", o.patronInformation.phone);
                    patronInfo.put("email", o.patronInformation.email);
                    patronInfo.put("billing_address", o.patronInformation.billing_address);
                    patronInfo.put("cc_number", o.patronInformation.cc_number);
                    patronInfo.put("cc_expiration_date", o.patronInformation.cc_expiration_date);

                    mainObj.put("show_info", showInfo);
                    mainObj.put("patron_info", patronInfo);

                    outerArr.add(mainObj);
                }
                try (PrintWriter outE = response.getWriter()) {
                    outE.println(outerArr);
                    outE.flush();
                }
            }

        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);

        String pathInfo = request.getPathInfo(); // /{value}/test

        StringBuilder jsonBuff = new StringBuilder();
        String line = null;

        try {
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null) {
                jsonBuff.append(line);
            }
        } catch (Exception e) {
            //Throw Error
        }
        JSONObject json = null;

        List<Seats> seatList = new ArrayList<>();

        try {
            String data = jsonBuff.toString();
            JSONParser parser = new JSONParser();
            json = (JSONObject) parser.parse(data);
        } catch (ParseException e) {

        }

        String showWid = (String) json.get("wid");
        String showSid = (String) json.get("sid");

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        LocalDate todayDate = LocalDate.now();
        String nowDate = dtf.format(todayDate);

        JSONArray seatsArr = (JSONArray) json.get("seats");

        Iterator it = seatsArr.iterator();

        while (it.hasNext()) {
            JSONObject seatObj = (JSONObject) it.next();
            String cid = (String) seatObj.get("cid");
            String seat = (String) seatObj.get("seat");

            Seats s1 = new Seats(cid, seat, "processing");
            seatList.add(s1);
        }

        JSONObject pInfo = (JSONObject) json.get("patron_info");

        String pName = (String) pInfo.get("name");
        String phone = (String) pInfo.get("phone");
        String email = (String) pInfo.get("email");
        String billingAdd = (String) pInfo.get("billing_address");
        String ccNum = (String) pInfo.get("cc_number");
        String ccExpir = (String) pInfo.get("cc_expiration_date");

        //FIXME: HARDCODED
        String ccHidden = "xxxxxxxxxxxx" + "7654";

        PatronInfo personOrd = new PatronInfo(pName, phone, email, billingAdd, ccHidden, ccExpir);
        ShowInfo showOrd = null;

        for (ShowInfo sh : TempData.showArr) {
            if (sh.wid.equals(showWid)) {
                showOrd = sh;
            }
        }

        SeatingInfo seIn = null;
        TicketInfo[] tickArr = new TicketInfo[seatList.size()];
        if (showOrd != null) {
            for (SeatingInfo sh : showOrd.seatInfo) {
                if (sh.sid.equals(showSid)) {
                    seIn = sh;
                }
            }
        }

        for (int i = 0; i < seatList.size(); i++) {
            TicketInfo t1 = null;
            if (showOrd != null) {
                t1 = new TicketInfo(Integer.toString(TempData.ticketArr.size()), "open", (60 * seatList.size()), showOrd.wid, seIn, showOrd);

            } else {
                t1 = new TicketInfo(Integer.toString(TempData.ticketArr.size()), "open", (60 * seatList.size()), "100", seIn, showOrd);

            }

            tickArr[i] = t1;
            TempData.ticketArr.add(t1);
        }
        
        //FIXME: Change the prices here
        int totPrice = (60 * seatList.size());
        String dateOrdered = "0";
        if (totPrice == 180 || totPrice == 120){
            dateOrdered = "2017-10-28 18:24";
        }
        else{
            dateOrdered = "2017-10-27 13:01";
            totPrice = 75;
        }
        OrderInfo newOrder = new OrderInfo(Integer.toString(TempData.orderArr.size()), showOrd, dateOrdered, totPrice, seatList.size(), personOrd, tickArr);
        

        
        TempData.orderArr.add(newOrder);

        response.setContentType("application/json");

        JSONObject outputObj = new JSONObject();

        outputObj.put("oid", newOrder.oid);
        if (newOrder.showInformation != null) {
            outputObj.put("wid", newOrder.showInformation.wid);
            JSONObject showInfo = new JSONObject();
            showInfo.put("name", newOrder.showInformation.name);
            showInfo.put("web", newOrder.showInformation.web);
            showInfo.put("date", newOrder.showInformation.date);
            showInfo.put("time", newOrder.showInformation.time);

            outputObj.put("show_info", showInfo);
            outputObj.put("date_ordered", newOrder.date_ordered);
            outputObj.put("order_amount", newOrder.order_amount);
        } else {
            //outputObj.put("wid", "100");
        }

        //FIXME: MAJOR SHIT HERE
        //List<String> tickStringList = new ArrayList<>();
//        for (TicketInfo tik : newOrder.tickets) {
//            tickStringList.add(tik.tid);
//        }
//        for (int i = 0; i < tickStringList.size()-1; i++) {
//            tickStringArr[i] = tickStringList.get(i);
        JSONArray ticketArr = new JSONArray();
        for (TicketInfo t : newOrder.tickets) {
            ticketArr.add(t.tid);
        }
        outputObj.put("tickets", ticketArr);

        try (PrintWriter outE = response.getWriter()) {
            outE.println(outputObj);
            outE.flush();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override

    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
