/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javaCode.OrderInfo;
import javaCode.PatronInfo;
import javaCode.SeatingInfo;
import javaCode.Rows;
import javaCode.Seats;
import javaCode.ShowInfo;
import javaCode.TempData;
import javaCode.TicketInfo;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author alex
 */
@WebServlet(name = "Seating", urlPatterns = {"/thalia/seating/*"})
public class Seating extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);

        if (TempData.counterSeating == 0) {
            TempData.createStuff();
            TempData.counterSeating++;
        } else {
            TempData.counterSeating++;
        }

        String pathInfo = request.getPathInfo(); // /{value}/test

        if (pathInfo != null) {
            String[] pathParts = pathInfo.split("/");
            int numParts = pathParts.length;

            if (numParts == 2 && pathParts[1] != null) {
                String sectionNum = pathParts[1];

                for (SeatingInfo s : TempData.seatArr) {
                    if (s.sid.equals(sectionNum)) {
                        JSONObject oxj = new JSONObject();

                        oxj.put("sid", s.sid);
                        oxj.put("section_name", s.name);

                        JSONArray oxArr = new JSONArray();

                        for (Rows so : s.section) {
                            JSONObject insideOxArr = new JSONObject();
                            JSONArray seatArr = new JSONArray();
                            insideOxArr.put("row", so.row);

//                            for (String seatIn : so.seat) {
//                                seatArr.add(seatIn);
//                            }
                            insideOxArr.put("seats", seatArr);

                            oxArr.add(insideOxArr);
                        }

                        oxj.put("seating", oxArr);
                        try (PrintWriter outE = response.getWriter()) {
                            outE.println(oxj);
                            outE.flush();
                        }
                    }
                }

            }
        } else {

            String showWidParam = request.getParameter("show");
            String sectionParam = request.getParameter("section");
            String countParam = request.getParameter("count");
            String startingSeat = request.getParameter("starting_seat_id");

            if (showWidParam != null && sectionParam != null && countParam != null) {

                JSONObject showObj = new JSONObject();

                for (ShowInfo sh : TempData.showArr) {
                    if (sh.wid.equals(showWidParam)) {
                        boolean found = false;
                        JSONObject show_info = new JSONObject();

                        show_info.put("name", sh.name);
                        show_info.put("web", sh.web);
                        show_info.put("date", sh.date);
                        show_info.put("time", sh.time);

                        showObj.put("wid", sh.wid);
                        showObj.put("show_info", show_info);

                        for (SeatingInfo se : TempData.seatArr) {
                            if (se.sid.equals(sectionParam)) {
                                showObj.put("sid", se.sid);
                                showObj.put("section_name", se.name);

                                Rows realRow = null;
                                List<Seats> foundSeats = new ArrayList<>();
                                loops:
                                for (Rows r : se.section) {
                                    int counter = 0;
                                    foundSeats = new ArrayList<>();
                                    
                                    for (Seats seatRow : r.seatList) {
                                        
                                        if (seatRow.status.equals("available")) {
                                            if (startingSeat != null && sectionParam.equals(se.sid) && Integer.parseInt(seatRow.cid) < Integer.parseInt(startingSeat)) {
                                                //COME HERE aa
                                            } else {
                                                foundSeats.add(seatRow);
                                                counter++;
                                                if (counter == Integer.parseInt(countParam)) {
                                                    found = true;
                                                    realRow = new Rows(r.row, foundSeats);
                                                    break loops;
                                                }
                                            }

                                        } else {
                                            foundSeats = new ArrayList<>();
                                            counter = 0;
                                        }
                                    }
                                }

                                if (found && realRow != null) {

                                    if (startingSeat != null) {
                                        showObj.put("starting_seat_id", startingSeat);
                                    } else {
                                        showObj.put("starting_seat_id", realRow.seatList.get(0).cid);
                                    }

                                    showObj.put("status", "ok");

                                    //FIXME: this price is wrong
                                    int totPrice = 60 * realRow.seatList.size();

                                    showObj.put("total_amount", totPrice);

                                    JSONArray seatingArr = new JSONArray();
                                    JSONObject seatingObj = new JSONObject();
                                    seatingObj.put("row", realRow.row);

                                    JSONArray seatsArr = new JSONArray();

                                    for (Seats sx : realRow.seatList) {
                                        JSONObject seatsObj = new JSONObject();
                                        seatsObj.put("cid", sx.cid);
                                        seatsObj.put("seat", sx.seatNum);
                                        seatsObj.put("status", sx.status);
                                        seatsArr.add(seatsObj);
                                    }
                                    seatingObj.put("seats", seatsArr);
                                    seatingArr.add(seatingObj);
                                    showObj.put("seating", seatingArr);

                                } else {
                                    showObj.put("starting_seat_id", "201");
                                    showObj.put("status", "Error: " + countParam + " contiguous seats not available");

                                    JSONArray blankSeating = new JSONArray();
                                    showObj.put("seating", blankSeating);

                                }
                            }
                        }
                        
                        try (PrintWriter outE = response.getWriter()) {
                            outE.println(showObj);
                            outE.flush();
                        }

                    }

                    //FIXME: add error if WID doesn't exist (send 404)
                }
            } else {
                JSONObject obj = new JSONObject();
                JSONArray arr = new JSONArray();
                for (SeatingInfo s : TempData.seatArr) {
                    obj = new JSONObject();
                    obj.put("sid", s.sid);
                    obj.put("section_name", s.name);
                    arr.add(obj);
                }
                try (PrintWriter outE = response.getWriter()) {
                    outE.println(arr);
                    outE.flush();
                }
            }

        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
