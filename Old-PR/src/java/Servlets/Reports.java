/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javaCode.OrderInfo;
import javaCode.PatronInfo;
import javaCode.ReportInfo;
import javaCode.SeatingInfo;
import javaCode.Rows;
import javaCode.ShowInfo;
import javaCode.TempData;
import javaCode.TicketInfo;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author alex
 */
@WebServlet(name = "Reports", urlPatterns = {"/thalia/reports/*"})
public class Reports extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        if (TempData.counterSeating == 0) {
            TempData.createStuff();
            TempData.counterSeating++;
        } else {
            TempData.counterSeating++;
        }

        ReportInfo r1 = new ReportInfo("900", "Test Report 1", "11-11-2015", "11-15-2015", 35, 255, 125, "70.1%", TempData.showArr);

        TempData.reportArr.add(r1);
        JSONArray mainArr = new JSONArray();
        for (ReportInfo r : TempData.reportArr) {
            JSONObject mainObj = new JSONObject();

            mainObj.put("mrid", r.mrid);
            mainObj.put("name", r.reportName);

            mainArr.add(mainObj);

        }
        try (PrintWriter outE = response.getWriter()) {
            outE.println(mainArr);
            outE.flush();
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
