/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import javaCode.DonationInfo;
import javaCode.OrderInfo;
import javaCode.PatronInfo;
import javaCode.SeatingInfo;
import javaCode.Rows;
import javaCode.ShowInfo;
import javaCode.TempData;
import javaCode.TicketInfo;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author alex
 */
@WebServlet(name = "Tickets", urlPatterns = {"/thalia/tickets/*"})
public class Tickets extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        if (TempData.counterSeating == 0) {
            TempData.createStuff();
            TempData.counterSeating++;
        } else {
            TempData.counterSeating++;
        }

        String pathInfo = request.getPathInfo(); // /{value}/test

        if (pathInfo != null) {

            String[] pathParts = pathInfo.split("/");
            int numParts = pathParts.length;

            if (numParts == 2 && pathParts[1] != null) {
                String ticketID = pathParts[1];

                JSONObject mainObj = new JSONObject();
                for (OrderInfo o : TempData.orderArr) {
                    for (javaCode.TicketInfo t : o.tickets) {
                        if (ticketID.equals(t.tid)) {
                            mainObj.put("tid", t.tid);
                            mainObj.put("price", t.price);
                            mainObj.put("status", t.status);
                            mainObj.put("wid", t.wid);

                            JSONObject showInfo = new JSONObject();
                            if (o.showInformation != null) {
                                showInfo.put("name", o.showInformation.name);
                                showInfo.put("web", o.showInformation.web);
                                showInfo.put("date", o.showInformation.date);
                                showInfo.put("time", o.showInformation.time);
                            }

                            mainObj.put("show_info", showInfo);

                            JSONObject patronInfo = new JSONObject();

                            patronInfo.put("name", o.patronInformation.name);
                            patronInfo.put("phone", o.patronInformation.phone);
                            patronInfo.put("email", o.patronInformation.email);
                            patronInfo.put("billing_address", o.patronInformation.billing_address);
                            patronInfo.put("cc_number", o.patronInformation.cc_number);
                            patronInfo.put("cc_expiration_date", o.patronInformation.cc_expiration_date);

                            mainObj.put("patron_info", patronInfo);

                            if (t.seatInfo != null) {
                                mainObj.put("sid", t.seatInfo.sid);
                                mainObj.put("section_name", t.seatInfo.name);

                            }

                            JSONArray seatingArr = new JSONArray();

                            JSONArray seatArr = new JSONArray();

                            if (t.seatInfo != null) {
                                for (Rows sx : t.seatInfo.section) {

                                    JSONObject seatingObj = new JSONObject();
                                    seatingObj.put("row", sx.row);

                                    JSONObject seatObj = new JSONObject();

//                                seatObj.put("cid", sx.cid);
//                                seatObj.put("seat", sx.seat);
                                    seatArr.add(seatObj);
                                    seatingObj.put("seats", seatArr);
                                    seatingArr.add(seatingObj);
                                }
                            }

                            mainObj.put("seating", seatingArr);

                        }
                    }

                    try (PrintWriter outE = response.getWriter()) {

                        outE.println(mainObj);
                        outE.flush();
                    }

                }
            } else {

            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);

        String pathInfo = request.getPathInfo(); // /{value}/test
        StringBuilder jsonBuff = new StringBuilder();
        String line = null;
        JSONObject json = null;
        try {
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null) {
                jsonBuff.append(line);
            }
        } catch (Exception e) {
            //Throw Error
        }

        String[] pathParts = pathInfo.split("/");
        int numParts = pathParts.length;
        if (pathInfo != null) {
            if (numParts == 2 && pathParts[1] != null) {
                String wid = pathParts[1];

                String ticketID = pathParts[1];
                if (ticketID.equals("donations")) {
                    try {
                        String data = jsonBuff.toString();
                        JSONParser parser = new JSONParser();
                        json = (JSONObject) parser.parse(data);
                    } catch (ParseException e) {

                    }

                    JSONArray tikArr = (JSONArray) json.get("tickets");

                    Iterator it = tikArr.iterator();
                    while (it.hasNext()) {
                        String ticketId = (String) it.next();

                        for (DonationInfo d : TempData.donationArr) {
                            for (TicketInfo t : d.ticketIn) {
                                if (d.did.equals(ticketId)) {

                                    d.setStatus("assigned");
                                }
                            }
                        }

                    }

                    JSONObject mainObj = new JSONObject();

                    try (PrintWriter outE = response.getWriter()) {
                        outE.println(mainObj);
                        outE.flush();
                    }

                } else {
                    //FIXME: newTicketID could just be the pathParts[1];
                    String newTicketID = request.getParameter("tid");
                    String newTicketStatus = request.getParameter("status");

                    for (TicketInfo t : TempData.ticketArr) {
                        if (t.tid.equals(newTicketID) && newTicketStatus.equals("used")) {
                            t.setStatus("assigned");

                            JSONObject mainObj = new JSONObject();
                            mainObj.put("tid", t.tid);
                            mainObj.put("status", t.status);

                            try (PrintWriter outE = response.getWriter()) {
                                outE.println(mainObj);
                                outE.flush();
                            }
                        }
                    }
                }

            }

        } else {

        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
