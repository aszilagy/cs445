/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javaCode.DonationInfo;
import javaCode.OrderInfo;
import javaCode.PatronInfo;
import javaCode.SeatingInfo;
import javaCode.Rows;
import javaCode.Seats;
import javaCode.ShowInfo;
import javaCode.TempData;
import javaCode.TicketInfo;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author alex
 */
@WebServlet(name = "shows", urlPatterns = {"/thalia/shows/*"})
public class Shows extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);

        if (TempData.counterSeating == 0) {
            TempData.createStuff();
            TempData.counterSeating++;
        } else {
            TempData.counterSeating++;
        }

        JSONObject obj = new JSONObject();
        JSONArray outerArray = new JSONArray();

        String pathInfo = request.getPathInfo(); // /{value}/test  
        if (pathInfo != null) {

            String[] pathParts = pathInfo.split("/");
            int numParts = pathParts.length;

            if (numParts == 2 && pathParts[1] != null) {
                JSONObject showObj = new JSONObject();
                JSONArray seating_info = new JSONArray();
                String widPath = pathParts[1]; // {wid}

                for (ShowInfo sh : TempData.showArr) {
                    if (sh.wid.equals(widPath)) {

                        JSONObject show_info = new JSONObject();

                        show_info.put("name", sh.name);
                        show_info.put("web", sh.web);
                        show_info.put("date", sh.date);
                        show_info.put("time", sh.time);

                        showObj.put("wid", sh.wid);
                        showObj.put("show_info", show_info);

                        for (SeatingInfo se : TempData.seatArr) {
                            if (se.wid.equals(sh.wid)) {
                                //FOUND SEATING WITH SHOW

                                JSONObject seatSection = new JSONObject();
                                seatSection.put("sid", se.sid);
                                seatSection.put("price", se.price);
                                seating_info.add(seatSection);
                                showObj.put("seating_info", seating_info);
                            }
                            //Else return no seating? FIXME
                        }
                    }

                    //FIXME: add error if WID doesn't exist (send 404)
                }
                try (PrintWriter outE = response.getWriter()) {
                    outE.println(showObj);
                    //outE.println("\n" + widPath);
                    outE.flush();
                }
            } else if (numParts == 3 && pathParts[2].equals("sections")) {
                JSONObject showObj = new JSONObject();
                JSONArray seating_info = new JSONArray();
                String widPath = pathParts[1];
                for (ShowInfo sh : TempData.showArr) {
                    if (widPath.equals(sh.wid)) {
                        for (SeatingInfo se : sh.seatInfo) {

                            //FOUND SEATING WITH SHOW
                            JSONObject seatSection = new JSONObject();
                            seatSection.put("sid", se.sid);
                            seatSection.put("section_name", se.name);
                            seatSection.put("price", se.price);
                            seating_info.add(seatSection);

                            //Else return no seating? FIXME
                        }
                    }
                }

                try (PrintWriter outE = response.getWriter()) {
                    outE.println(seating_info);
                    //outE.println("\n" + widPath);
                    outE.flush();
                }
            } else if (numParts == 4 && pathParts[2].equals("sections")) {
                JSONObject showObj = new JSONObject();

                String widPath = pathParts[1];
                String sidPath = pathParts[3];

                for (ShowInfo sh : TempData.showArr) {
                    if (sh.wid.equals(widPath)) {

                        showObj.put("wid", sh.wid);

                        JSONObject show_info = new JSONObject();

                        show_info.put("name", sh.name);
                        show_info.put("web", sh.web);
                        show_info.put("date", sh.date);
                        show_info.put("time", sh.time);

                        showObj.put("show_info", show_info);

                        for (SeatingInfo seatIn : sh.seatInfo) {
                            if (sidPath.equals(seatIn.sid)) {
                                showObj.put("sid", seatIn.sid);
                                showObj.put("section_name", seatIn.name);
                                //showObj.put("price", seatIn.price);

                                JSONArray seatingArray = new JSONArray();

                                for (Rows sx : seatIn.section) {
                                    JSONObject seatingObj = new JSONObject();
                                    seatingObj.put("row", sx.row);

                                    JSONArray seatsArr = new JSONArray();

                                    for (Seats sz : sx.seatList) {
                                        JSONObject indivSeat = new JSONObject();
                                        indivSeat.put("cid", sz.cid);
                                        indivSeat.put("seat", sz.seatNum);
                                        indivSeat.put("status", sz.status);
                                        seatsArr.add(indivSeat);
                                    }
                                    seatingObj.put("seats", seatsArr);
                                    seatingArray.add(seatingObj);
                                }

                                showObj.put("seating", seatingArray);
                            }
                        }
                    }
                    //FIXME: add error if WID doesn't exist (send 404)
                }
                try (PrintWriter outE = response.getWriter()) {
                    outE.println(showObj);
                    outE.flush();
                }

            } else if (numParts == 4 && pathParts[2].equals("donations")) {
                JSONObject showObj = new JSONObject();

                String widPath = pathParts[1];
                String did = pathParts[3];
                for (DonationInfo d : TempData.donationArr) {
                    if (did.equals(d.did)) {
                        showObj.put("did", d.did);
                        showObj.put("wid", d.ticketIn[0].wid);
                        showObj.put("count", d.countReq);
                        showObj.put("status", d.donationStatus);

                        JSONArray ticketArr = new JSONArray();
                        if (d.donationStatus.equals("pending")) {
                            //Leave array empty
                        } else if (d.donationStatus.equals("assigned")) {
                            for (TicketInfo t : d.ticketIn) {
                                ticketArr.add(t.tid);
                            }
                        }
                        showObj.put("tickets", ticketArr);

                        JSONObject patronInfo = new JSONObject();
                        patronInfo.put("name", d.patronIn.name);
                        patronInfo.put("phone", d.patronIn.phone);
                        patronInfo.put("email", d.patronIn.email);
                        patronInfo.put("billing_address", d.patronIn.billing_address);
                        patronInfo.put("cc_number", d.patronIn.cc_number);
                        patronInfo.put("cc_expiration_date", d.patronIn.cc_expiration_date);

                        showObj.put("patron_info", patronInfo);

                    }
                }

                try (PrintWriter outE = response.getWriter()) {
                    outE.println(showObj);
                    //outE.println("\n" + widPath);
                    outE.flush();
                }
            }

        } else {

            for (ShowInfo shi : TempData.showArr) {
                JSONObject showObj = new JSONObject();
                JSONObject show_info = new JSONObject();

                show_info.put("name", shi.name);
                show_info.put("web", shi.web);
                show_info.put("date", shi.date);
                show_info.put("time", shi.time);

                showObj.put("wid", shi.wid);
                showObj.put("show_info", show_info);
                outerArray.add(showObj);
            }
            try (PrintWriter outE = response.getWriter()) {
                outE.println(outerArray);
                //outE.println("\n" + widPath);
                outE.flush();
            }
        }
        //doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);

        String pathInfo = request.getPathInfo(); // /{value}/test

        StringBuilder jsonBuff = new StringBuilder();
        String line = null;

        try {
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null) {
                jsonBuff.append(line);
            }
        } catch (Exception e) {
            //Throw Error
        }
        JSONObject json = null;
        if (pathInfo != null) {
            String[] pathParts = pathInfo.split("/");
            int numParts = pathParts.length;

            if (numParts == 3 && pathParts[2] != null) {
                if (pathParts[2].equals("donations")) {
                    String wid = pathParts[1];
                    try {
                        String data = jsonBuff.toString();
                        JSONParser parser = new JSONParser();
                        json = (JSONObject) parser.parse(data);
                    } catch (ParseException e) {

                    }

                    if (json != null) {
                        String newShowWid = (String) json.get("wid");

                        String newDonCount = (String) json.get("count").toString();

                        JSONObject patronObj = (JSONObject) json.get("patron_info");
                        String patronName = (String) patronObj.get("name");
                        String patronPhone = (String) patronObj.get("phone");
                        String patronEmail = (String) patronObj.get("email");
                        String patronBilling = (String) patronObj.get("billing_address");
                        String patronCCNum = (String) patronObj.get("cc_number");
                        String patronCCExpir = (String) patronObj.get("cc_expiration_date");

                        PatronInfo p = new PatronInfo(patronName, patronPhone, patronEmail, patronBilling, patronCCNum, patronCCExpir);

                        ShowInfo showOrd = null;

                        for (ShowInfo sh : TempData.showArr) {
                            if (sh.wid.equals(newShowWid)) {
                                showOrd = sh;
                            }
                        }
                        int newDonCountInt = Integer.parseInt(newDonCount);
                        TicketInfo[] tickArr = new TicketInfo[newDonCountInt];
                        SeatingInfo secTemp = null;
                        for (int i = 0; i < newDonCountInt; i++) {
                            TicketInfo t1 = null;
                            if (showOrd != null) {
                                t1 = new TicketInfo(Integer.toString(TempData.ticketArr.size()), "open", (60 * newDonCountInt), showOrd.wid, secTemp, showOrd);

                            }
                            else
                            {
                                 t1 = new TicketInfo(Integer.toString(TempData.ticketArr.size()), "open", (60 * newDonCountInt), "100", secTemp, showOrd);
                           
                            }
                            tickArr[i] = t1;
                            TempData.ticketArr.add(t1);
                        }

                        DonationInfo newDonation = new DonationInfo(Integer.toString(TempData.donationArr.size()), Integer.parseInt(newDonCount), "pending", tickArr, p);

                        TempData.donationArr.add(newDonation);

                        JSONObject outputObj = new JSONObject();

                        outputObj.put("did", Integer.toString(TempData.donationArr.size() - 1));
                        response.setContentType("application/json");

                        try (PrintWriter outE = response.getWriter()) {
                            outE.println(outputObj);
                            outE.flush();
                        }
                    }

                } else {
                    response.setContentType("application/json");
                    JSONObject outputObj = new JSONObject();

                    outputObj.put("wid", "308");
                    try (PrintWriter outE = response.getWriter()) {
                        outE.println(outputObj);
                        outE.flush();
                    }
                }

            }
        } else {

            List<SeatingInfo> seInfo = new ArrayList<SeatingInfo>();

            try {
                String data = jsonBuff.toString();
                JSONParser parser = new JSONParser();
                json = (JSONObject) parser.parse(data);
            } catch (ParseException e) {

            }

            JSONObject showInfoTest = (JSONObject) json.get("show_info");

            String newShowName = (String) showInfoTest.get("name");
            String newWebName = (String) showInfoTest.get("web");

            String newDate = (String) showInfoTest.get("date");
            String newTime = (String) showInfoTest.get("time");

            JSONArray seatingInfoTest = (JSONArray) json.get("seating_info");

            Iterator it = seatingInfoTest.iterator();

            while (it.hasNext()) {
                JSONObject obj = (JSONObject) it.next();
                String sidNo = (String) obj.get("sid");
                String sectionName = null;
                Rows[] emptySeats = new Rows[]{};
                //String priceNo = (String)obj.get("price");

                //String wi, int pri, String si, String na, Seats[] seatArr
                for (SeatingInfo sx : TempData.seatArr) {
                    if (sx.sid.equals(sidNo)) {
                        sectionName = sx.name;
                        emptySeats = sx.section;
                    }
                }

                SeatingInfo newSeatIn = new SeatingInfo("308", 0, sidNo, sectionName, emptySeats);
                seInfo.add(newSeatIn);
            }

            //FIXME: assign WID based on previous WID
            ShowInfo newShow = new ShowInfo("308", newShowName, newWebName, newDate, newTime, seInfo);
            TempData.showArr.add(newShow);

            response.setContentType("application/json");

            JSONObject outputObj = new JSONObject();

            outputObj.put("wid", "308");
            try (PrintWriter outE = response.getWriter()) {
                outE.println(outputObj);
                outE.flush();
            }
        }
    }

    @Override
    public void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pathInfo = request.getPathInfo(); // /{value}/test
        String[] pathParts = pathInfo.split("/");
        int numParts = pathParts.length;

        StringBuilder jsonBuff = new StringBuilder();
        String line = null;
        JSONObject json = null;
        try {
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null) {
                jsonBuff.append(line);
            }
        } catch (Exception e) {
            //Throw Error
        }

        if (numParts == 2 && pathParts[1] != null) {
            String wid = pathParts[1];

            for (ShowInfo s : TempData.showArr) {
                if (s.wid.equals(wid)) {
                    List<SeatingInfo> seInfo = new ArrayList<SeatingInfo>();

                    try {
                        String data = jsonBuff.toString();
                        JSONParser parser = new JSONParser();
                        json = (JSONObject) parser.parse(data);
                    } catch (ParseException e) {

                    }

                    JSONObject showInfoTest = (JSONObject) json.get("show_info");

                    String newShowName = (String) showInfoTest.get("name");
                    String newWebName = (String) showInfoTest.get("web");

                    String newDate = (String) showInfoTest.get("date");
                    String newTime = (String) showInfoTest.get("time");

                    JSONArray seatingInfoTest = (JSONArray) json.get("seating_info");

                    Iterator it = seatingInfoTest.iterator();

//                    while (it.hasNext()) {
//                        JSONObject obj = (JSONObject) it.next();
//                        String sidNo = (String) obj.get("sid");
//                        //String priceNo = (String)obj.get("price");
//                        
//                        for(SeatingInfo sx : s.seatInfo){
//                            if(sx.sid.equals(sidNo)){
//                                sx.price = 0;
//                            }
//                        }
//                        
//                    }
//                    s.name = newShowName;
//                    s.web = newWebName;
//                    s.date = newDate;
//                    s.time = newTime;
                    JSONObject outputObj = new JSONObject();

                    try (PrintWriter outE = response.getWriter()) {
                        outE.println(outputObj);
                        outE.flush();
                    }
                }
            }

        }
    }

//    @Override
//    protected void doPut(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//
//    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
