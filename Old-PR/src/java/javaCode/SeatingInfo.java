/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaCode;

/**
 *
 * @author alex
 */
public class SeatingInfo {

    public String wid;
    public int price;
    
    public String sid;
    public String name;
    public Rows[] section;

    public SeatingInfo(String wi, int pri, String si, String na, Rows[] seatArr) {
        wid = wi;
        price = pri;
        sid = si;
        name = na;
        section = seatArr;
    }
}
