/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaCode;

import java.util.*;

/**
 *
 * @author alex The purpose of this file is to store data and later serialize it
 */
public class TempData {

    public static List<ShowInfo> showArr = new ArrayList<>();
    public static List<SeatingInfo> seatArr = new ArrayList<>();
    public static List<OrderInfo> orderArr = new ArrayList<>();
    public static List<TicketInfo> ticketArr = new ArrayList<>();
    public static List<DonationInfo> donationArr = new ArrayList<>();
    public static List<ReportInfo> reportArr = new ArrayList<>();

    public static int counterSeating = 0;

    public static void createStuff() {
        Rows[] seatArray = new Rows[4];
        Rows[] seatArray2 = new Rows[4];

        List<Seats> seatOne = new ArrayList<>();
        Seats seat1 = new Seats("201", "1", "available");
        Seats seat2 = new Seats("202", "2", "available");
        Seats seat3 = new Seats("203", "3", "available");
        Seats seat4 = new Seats("204", "4", "available");

        List<Seats> seatTwo = new ArrayList<>();
        Seats seat5 = new Seats("204", "5", "available");
        Seats seat6 = new Seats("205", "6", "available");
        Seats seat7 = new Seats("206", "7", "available");
        Seats seat8 = new Seats("207", "8", "available");
        

        seatOne.add(seat1);
        seatOne.add(seat2);
        seatOne.add(seat3);
        seatOne.add(seat4);
        seatTwo.add(seat5);
        seatTwo.add(seat6);
        seatTwo.add(seat7);
        seatTwo.add(seat8);

        seatArray[0] = new Rows("1", seatOne);
        seatArray[1] = new Rows("2", seatOne);
        seatArray[2] = new Rows("3", seatOne);
        seatArray[3] = new Rows("4", seatOne);
        
        seatArray2[0] = new Rows("1", seatTwo);
        seatArray2[1] = new Rows("2", seatTwo);
        seatArray2[2] = new Rows("3", seatTwo);
        seatArray2[3] = new Rows("4", seatTwo);

        SeatingInfo sec1 = new SeatingInfo("01", 200, "123", "Front right", seatArray);
        SeatingInfo sec2 = new SeatingInfo("01", 400, "124", "Front center", seatArray2);
        SeatingInfo sec3 = new SeatingInfo("01", 200, "125", "Front left", seatArray);
        SeatingInfo sec4 = new SeatingInfo("01", 400, "126", "Main right", seatArray);
        SeatingInfo sec5 = new SeatingInfo("01", 200, "127", "Main center", seatArray);
        SeatingInfo sec6 = new SeatingInfo("01", 400, "128", "Main left", seatArray);

        TempData.seatArr.add(sec1);
        TempData.seatArr.add(sec2);
        TempData.seatArr.add(sec3);
        TempData.seatArr.add(sec4);
        TempData.seatArr.add(sec5);
        TempData.seatArr.add(sec6);

    }

}
