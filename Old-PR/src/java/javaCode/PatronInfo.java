/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaCode;

/**
 *
 * @author alex
 */
public class PatronInfo {
    public String name;
    public String phone;
    public String email;
    public String billing_address;
    public String cc_number;
    public String cc_expiration_date;
    
    public PatronInfo(String na, String ph, String em, String bi, String ccNum, String ccExp){
        name = na;
        phone = ph;
        email = em;
        billing_address = bi;
        cc_number = ccNum;
        cc_expiration_date = ccExp;
    }
}
