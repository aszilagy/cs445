/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaCode;

/**
 *
 * @author alex
 */
public class OrderInfo {
    public String oid;
    public ShowInfo showInformation;
    public String date_ordered;
    public int order_amount;
    public int numOfTickets;
    
    public PatronInfo patronInformation;
    public TicketInfo[] tickets;
    
    public OrderInfo(String oidNum, ShowInfo showIn, String dateOrder, int orderAmt, int numOfTick, PatronInfo pat, TicketInfo[] tik){
        oid = oidNum;
        showInformation = showIn;
        date_ordered = dateOrder;
        order_amount = orderAmt;
        numOfTickets = numOfTick;
        patronInformation = pat;
        tickets = tik;
    }

    public OrderInfo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
