javac -d classes -cp classes src/cs445/hw1/Thing.java
javac -d classes -cp classes src/cs445/hw1/Creature.java
javac -d classes -cp classes src/cs445/hw1/Flyer.java
javac -d classes -cp classes src/cs445/hw1/Fly.java
javac -d classes -cp classes src/cs445/hw1/Bat.java
javac -d classes -cp classes src/cs445/hw1/Tiger.java
javac -d classes -cp classes src/cs445/hw1/Ant.java
javac -d classes -cp classes src/cs445/hw1/TestCreature.java

javac -d test -cp .:classes:classes/cs445/hw1/junit-4.10.jar test/Junit_Test.java
