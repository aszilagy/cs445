import cs445.hw1.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alex
 */
public class Junit_Test {

    @Test
    public void test_test_creature(){
        TestCreature.main(null);
    }
    
    @Test
    public void test_thing_tostring() {
        Thing aThing = new Thing();
        aThing.toString(aThing);
    }

    @Test
    public void test_notThing_tostring() {
        Bat notAThing = new Bat("testBat");
        notAThing.toString(notAThing);
    }

    @Test
    public void test_create_tiger() {
        Tiger testTiger = new Tiger("testTiger");
        assertEquals("testTiger", testTiger.name);
    }

    @Test
    public void test_create_ant() {
        Ant testAnt = new Ant("testAnt");
        assertEquals("testAnt", testAnt.name);
    }

    @Test
    public void test_create_fly() {
        Fly testFly = new Fly("testFly");
        assertEquals("testFly", testFly.name);
    }

    @Test
    public void test_create_bat() {
        Bat testBat = new Bat("testBat");
        assertEquals("testBat", testBat.name);
    }

    @Test
    public void test_empty_stomach() {
        Tiger testTiger = new Tiger("testTiger");
        Thing[] Stomach = testTiger.whatDidYouEat();
        assertEquals(null, Stomach[0]);
    }

    @Test
    public void test_tiger_eat() {
        Tiger testTiger = new Tiger("testTiger");
        Ant toBeEaten = new Ant("testAnt");
        testTiger.eat(toBeEaten);
        Thing[] Stomach = testTiger.whatDidYouEat();
        assertEquals(toBeEaten, Stomach[0]);
    }

    @Test
    public void test_ant_eat() {
        Ant testAnt = new Ant("testAnt");
        Bat toBeEaten = new Bat("testBat");
        testAnt.eat(toBeEaten);
        Thing[] Stomach = testAnt.whatDidYouEat();
        assertEquals(toBeEaten, Stomach[0]);
    }

    @Test
    public void test_fly_eat_fail() {
        Fly testFly = new Fly("testFly");
        Bat toBeEaten = new Bat("testBat");
        testFly.eat(toBeEaten);
        Thing[] Stomach = testFly.whatDidYouEat();
        assertEquals(null, Stomach[0]);
    }

    @Test
    public void test_fly_eat() {
        Fly testFly = new Fly("testFly");
        Thing toBeEaten = new Thing();
        testFly.eat(toBeEaten);
        Thing[] Stomach = testFly.whatDidYouEat();
        assertEquals(toBeEaten, Stomach[0]);
    }

    @Test
    public void test_bat_eat_creature() {
        Bat testBat = new Bat("testBat");
        Ant toBeEaten = new Ant("testAnt");
        testBat.eat(toBeEaten);
        Thing[] Stomach = testBat.whatDidYouEat();
        assertEquals(toBeEaten, Stomach[0]);
    }

    @Test
    public void test_bat_eat_thing() {
        Bat testBat = new Bat("testBat");
        Thing toBeEaten = new Thing();
        testBat.eat(toBeEaten);
        Thing[] Stomach = testBat.whatDidYouEat();
        assertEquals(null, Stomach[0]);
    }

    @Test
    public void test_bat_fly() {
        Bat testBat = new Bat("testBat");
        testBat.move();
    }

    @Test
    public void test_ant_move() {
        Ant testAnt = new Ant("testAnt");
        testAnt.move();
    }

    @Test
    public void test_tiger_move() {
        Tiger testTiger = new Tiger("testAnt");
        testTiger.move();
    }

    @Test
    public void test_fly_move() {
        Fly testFly = new Fly("testFly");
        testFly.move();
    }
}
