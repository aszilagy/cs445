To compile:
	Navigate to base folder

Type this command into command prompt to compile files:
	bash Compile

To run TestCreature, run the following command:
	bash RunTestCreature

or alternatively:
	java -cp classes cs445.hw1.TestCreature

To run the Unit Test, run the following command:
	bash RunUnitTest

Note: You must have completed the "bash Compile" step before running this as the javac for the unit test lies in the Compile command.

Navigation:
	~/src/cs445/hw1/    	Contains the .java files for this project
	~/classes/cs445/hw1/    Contains the .class files for this project (after compile)
	~/test			Contains the Unit Test for this project (jUnit)

	README.txt		ReadMe file for the HW (Build and compile steps in here)
	Compile.txt		To be used for 'bash Compile' Compiles all source files
	RunTestCreature		To be used for 'bash RunTestCreature' - Runs 'TestCreature.java'
	RunUnitTest		To be used for 'bash RunUnitTest' - Runs 'Junit_Test.java'
