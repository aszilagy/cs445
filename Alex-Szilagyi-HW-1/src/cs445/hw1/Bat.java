package cs445.hw1;

/**
 *
 * @author Alex
 */
public class Bat extends Creature implements Flyer {

    public Bat(String name) {
        super(name);
    }

    @Override
    public void eat(Thing aThing) {
        String className = aThing.getClass().getSimpleName();
        if (className.equals("Thing")) {
            System.out.println(this.name + "" + className + " won't eat a " + aThing.name);
        } else if (!className.equals("Thing")) {
            super.eat(aThing);
        } else {
            //Remain Silent and don't eat
        }
    }

    @Override
    public void move() {
        fly();
    }

    //FIXME: Move this to interface FLYER to implement fly()
    public void fly() {
        String className = getClass().getSimpleName();
        System.out.println(this.name + " " + className + " is buzzing around in flight.");
    }

}
