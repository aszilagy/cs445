package cs445.hw1;

/**
 *
 * @author Alex Szilagyi
 *
 */
public class TestCreature {

    public static final int CREATURE_COUNT = 0;
    public static final int THING_COUNT = 5;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Thing[] thingArr = new Thing[THING_COUNT];
        int count = 0;

        for (int z = 0; z < THING_COUNT; z++) {
            Thing newThing = new Thing();
            thingArr[z] = newThing;
        }

        for (Thing t : thingArr) {
            t.name = "someName " + Integer.toString(count);
            count++;
        }

        for (Thing t : thingArr) {
            System.out.println(t.toString(t)); //@TODO: Don't need to call toString every time, can just print from array
        }

        testTiger();
    }

    private static void testTiger() {
        Tiger tigerOne = new Tiger("Tiger1");
        Ant antOne = new Ant("Ant1");
        Fly flyOne = new Fly("Fly1");
	Bat batOne = new Bat("Bat1");

        Thing thingOne = new Thing();
        thingOne.name = "Thing1";

        Thing[] tempThingArr;

        tempThingArr = new Thing[]{
            tigerOne,
            antOne,
            flyOne,
            thingOne,
	    batOne
        };

        for (Thing t : tempThingArr) {
            System.out.println(t.name);
        }
        
        tigerOne.move();
        antOne.move();
        flyOne.move();
	batOne.move();
        
        System.out.println("\n");
        
        tigerOne.eat(antOne);    
        antOne.eat(thingOne);
        flyOne.eat(thingOne);
	batOne.eat(thingOne);
        
        System.out.println("\n");
        
        tigerOne.whatDidYouEat();
        antOne.whatDidYouEat();
        flyOne.whatDidYouEat();
	batOne.whatDidYouEat();
            
    }
}
