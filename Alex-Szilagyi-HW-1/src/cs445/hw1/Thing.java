package cs445.hw1;

/**
 *
 * @author Alex
 */
public class Thing {

    public String name;

    public String toString(Object obj) {
        String className = getClass().getSimpleName();
        if (className.equals("Thing")) {
            return "Thing name is: " + this.name;
        }
        else
        {
            return "Object name is: " + this.name + " Class Type is: " + obj.getClass() + "\n";
        }
    }
}
