import java.util.Random;

/**
 *
 * @author Alex
 */
public class ImprovedRandom extends Random {



    public static void main(String[] args) {
        int x = randomBetween(5, 8);
        System.out.println(x);
        
    }

    public static int randomBetween(int min, int max) {
        Random rand = new Random();
        
        int result = min + rand.nextInt((max - min) + 1);

        return result;
    }
}
