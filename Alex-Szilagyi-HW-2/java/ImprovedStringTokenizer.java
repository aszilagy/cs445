/**
 *
 * @author Alex Szilagyi
 */
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class ImprovedStringTokenizer extends StringTokenizer {

    public ImprovedStringTokenizer(String str, String delim, boolean returnDelims) {
        super(str, delim, returnDelims);
    }

    public static void main(String[] args) {
        String[] newArr = printArray("This is a test");
        
        for(String s: newArr)
        {
            System.out.println(s);
        }
    }

    public static String[] printArray(String toArray) {
        StringTokenizer st = new StringTokenizer(toArray, " ");
        
        List<String> tempHolder = new ArrayList<>();
        
        int count = 0;
        while(st.hasMoreTokens())
        {
            tempHolder.add(st.nextToken());
            count++;
        }
        
        String[] newArray = new String[count];
        
        for(int i = 0; i < count; i++)
        {
            newArray[i] = tempHolder.get(i);
        }
        
        return newArray;
    }
}
