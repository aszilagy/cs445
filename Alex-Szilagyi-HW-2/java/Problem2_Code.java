public class A implements B
{
	
}

public class C extends A implements D
{
	
}

public class E extends C
{
	
}

public interface B
{
	
}

public interface D extends B
{
	
}

public class F
{
	
}