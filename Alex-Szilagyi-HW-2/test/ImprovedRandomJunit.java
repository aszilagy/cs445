/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 *
 * @author Alex
 */
public class ImprovedRandomJunit {

    public ImprovedRandomJunit() {
    }

    @Test
    public void test_min_greater_than_max_FAIL() throws Exception {
        int min = 8;
        int max = 5;
        int x = 0;
        ImprovedRandom r = new ImprovedRandom();

        try {
            x = r.randomBetween(min, max);
        } catch (Exception ex) {
            System.out.println("Test 1 PASSED: Exception thrown");
        }
    }

    @Test
    public void test_min_less_than_max_PASS() throws Exception {
        int min = 1;
        int max = 5;
        int x = 0;
        ImprovedRandom r = new ImprovedRandom();
        try {
            x = r.randomBetween(min, max);
            System.out.println("Test 2 PASSED: " + x);
        } catch (Exception ex) {
            System.out.println("test_min_less_than_max FAILED: Exception thrown");
        }
    }

    @Test
    public void test_within_bounds_PASS() {
        ImprovedRandom r = new ImprovedRandom();
        try {
            int x = r.randomBetween(5, 8);
            switch (x) {
                case 5:
                    assertEquals(x, 5);
                    break;
                case 6:
                    assertEquals(x, 6);
                    break;
                case 7:
                    assertEquals(x, 7);
                    break;
                case 8:
                    assertEquals(x, 8);
                    break;
                default:
                    assertEquals(1, 2); //FAIL if out of range
                    break;
            }

        } catch (Exception ex) {
            System.out.println("Test 3 FAILED: " + ex);
        }
    }
}
