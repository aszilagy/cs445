/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alex
 */
public class ImprovedStringTokenizerJunit {

    public ImprovedStringTokenizerJunit() {
    }

    @Test
    public void test_array_PASS() {
        String[] newArr = ImprovedStringTokenizer.printArray("This is a test");

        assertEquals(newArr[0], "This");
        assertEquals(newArr[1], "is");
        assertEquals(newArr[2], "a");
        assertEquals(newArr[3], "test");

    }

}
