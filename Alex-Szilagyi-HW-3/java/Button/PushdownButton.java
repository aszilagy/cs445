package Button;

import LightBulb.LightBulb;

/**
 *
 * @author Alex
 */
public class PushdownButton extends LightBulb {

    /** Check state of LightBulb, if its state is 0 it is off so turn it on
     *  if it's state is 1, it is already on so turn it off
     */
    public void PushButton() {
        if (getState() == 0) {
            System.out.println("Pushed Button is PUSHED");
            on();
        } else {
            System.out.println("Pushed Button is PUSHED");
            off();
        }
    }
}
