package Button;

import LightBulb.LightBulb;

/**
 *
 * @author Alex
 */
public class Button extends LightBulb{

    public void switchOn(){
        System.out.println("Button switched to ON");
        on();
    }
    
    public void switchOff(){
        System.out.println("Button switched to OFF");
        off();
    }
}
