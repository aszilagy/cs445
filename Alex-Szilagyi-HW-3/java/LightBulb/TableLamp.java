package LightBulb;

/**
 *
 * @author Alex Szilagyi
 *
 */
public interface TableLamp {
    public void on();
    public void off();
    
}
