package LightBulb;

/**
 *
 * @author Alex
 */
public class LightBulb implements TableLamp{

    int state = 0; //Lightbulb starts off, this will be used for testing purposes

    @Override
    public void on() {
        System.out.println("Lightbulb on");
        state = 1;
    }

    @Override
    public void off() {
        System.out.println("Lightbulb off");
        state = 0;
    }

    public int getState() {
        return state;
    }
}
