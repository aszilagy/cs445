import Button.Button;
import Button.PushdownButton;
import LightBulb.LightBulb;
import org.junit.Test;
import static org.junit.Assert.*;
import LightBulb.TableLamp;

/**
 *
 * @author Alex
 */
public class TestLamp {

    public static void main(String[] args) {
        Button b = new Button();
        b.switchOn();
        b.switchOff();        
        
        TableLamp t = new LightBulb();
        t.on();
        t.off();
        
    }

    public TestLamp() {

    }

    @Test
    public void test_lightbulb_on() {
        LightBulb l = new LightBulb();
        l.on();
        assertEquals(1, l.getState());
    }

    @Test
    public void test_lightbulb_off() {
        LightBulb l = new LightBulb();
        l.on();
        l.off();
        assertEquals(0, l.getState());
    }

    @Test
    public void test_start_off() {
        LightBulb l = new LightBulb();
        assertEquals(0, l.getState());
    }

    @Test
    public void test_button_on() {
        Button b = new Button();
        b.switchOn();
        assertEquals(1, b.getState());
    }

    @Test
    public void test_button_start_off() {
        Button b = new Button();
        assertEquals(0, b.getState());
    }

    @Test
    public void test_pushButton_start_off() {
        PushdownButton b = new PushdownButton();
        assertEquals(0, b.getState());
    }

    @Test
    public void test_pushButton_on() {
        PushdownButton b = new PushdownButton();
        b.PushButton();
        assertEquals(1, b.getState());
    }

    @Test
    public void test_pushButton_off() {
        PushdownButton b = new PushdownButton();
        b.PushButton();
        b.PushButton();
        assertEquals(0, b.getState());
    }

    @Test
    public void test_pushButton_on_twice() {
        PushdownButton b = new PushdownButton();
        b.PushButton();
        b.PushButton();
        b.PushButton();
        assertEquals(1, b.getState());
    }
}
