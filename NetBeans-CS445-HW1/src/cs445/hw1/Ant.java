package cs445.hw1;

/**
 *
 * @author Alex
 */
public class Ant extends Creature{

    public Ant(String name) {
        super(name);
    }

    @Override
    public void move() {
        String className = getClass().getSimpleName();
        System.out.println(this.name + " " + className + " is crawling around.");
    }
}
