package cs445.hw1;

/**
 *
 * @author Alex
 */
public class Tiger extends Creature {
    public Tiger(String name) {
        super(name);
    }

    @Override
    public void move() {
        String className = getClass().getSimpleName();
        System.out.println(this.name + " " + className + " has just pounced.");
    }
}
