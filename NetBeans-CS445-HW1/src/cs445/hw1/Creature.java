package cs445.hw1;

/**
 *
 * @author Alex
 */
public abstract class Creature extends Thing {

    Thing[] stomach = new Thing[1];

    public Creature(String name) {
        this.name = name;
    }

    public void eat(Thing aThing) {
        System.out.println(getClass() + " has just eaten a " + aThing.name);
        stomach[0] = aThing;
    }

    public abstract void move();

    /* Make the Creature tell what is in its stomach
    * If there is nothing in its stomach, whatDidYouEat() prints '{name} {class} has had nothing to eat!' 
    * If it has something in its stomach, whatDidYouEat() prints '{creature name} {class name} has eaten a {content of stomach}!' 
    * NOTE: The pattern {word} in the text above indicates what attribute value belongs in the output text.
     */
    public Thing[] whatDidYouEat() {
        if (stomach[0] == null) {
            System.out.println(this.name + getClass() + " has had nothing to eat!");
            return stomach;
        } else {
            System.out.println(this.name + getClass() + " has eaten a " + stomach[0].name);
            return stomach;
        }
    }
}
