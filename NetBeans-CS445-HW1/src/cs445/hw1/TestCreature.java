package cs445.hw1;

/**
 *
 * @author Alex Szilagyi
 *
 */
public class TestCreature {

    public static final int CREATURE_COUNT = 5;
    public static final int THING_COUNT = 6;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Thing[] thingArr = new Thing[THING_COUNT];
        int count = 0;

        for (int z = 0; z < THING_COUNT; z++) {
            Thing newThing = new Thing();
            thingArr[z] = newThing;
        }
        
        for (Thing t : thingArr) {
            t.name = "someName" + Integer.toString(count);
            count++;
        }

        System.out.println("Things:\n");
        for (Thing t : thingArr) {
            System.out.println(t.name);
        }
        System.out.println("\n");
        testCreature();
    }

    private static void testCreature() {
        Tiger tigerOne = new Tiger("Tiger1");
        Ant antOne = new Ant("Ant1");
        Fly flyOne = new Fly("Fly1");
        Tiger tigerTwo = new Tiger("Tiger2");

        Thing[] tempCreatureArr;

        tempCreatureArr = new Thing[]{
            tigerOne,
            antOne,
            flyOne,
            tigerTwo
        };
        System.out.println("Creatures:\n");
        for (Thing t : tempCreatureArr) {
            System.out.println(t.name);
        }
        System.out.println("\n");

    }
}
