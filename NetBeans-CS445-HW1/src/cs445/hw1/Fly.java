package cs445.hw1;

/**
 *
 * @author Alex
 */
public class Fly extends Creature implements Flyer {

    public Fly(String name) {
        super(name);
    }

    @Override
    public void eat(Thing aThing) {
        String className = aThing.getClass().getSimpleName();
        if (className.equals("Thing")) {
            super.eat(aThing);
        } else {
            System.out.println(this.name + "" + className + " won't eat a " + aThing.name);
        }
    }

    @Override
    public void move() {
        fly();
    }

    @Override
    public void fly() {
        String className = getClass().getSimpleName();
        System.out.println(this.name + " " + className + " is buzzing around in flight.");
    }

}
